﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_porfolio
{
    internal class cardArray
    {
        public static Card SearchCard(List<Card> _cards, string _Number, string _Pin)
        {
            foreach (Card c in _cards)
            {
                if (validation.ValidateNumber(_Number) == true || validation.ValidateNumber(_Pin) == true)
                {
                    if (validation.ValidateCard(validation.stringToNumber(_Number), validation.stringToNumber(_Pin)))
                    {
                        return c;
                    }
                }
            }

            return null;
        }
    }
}