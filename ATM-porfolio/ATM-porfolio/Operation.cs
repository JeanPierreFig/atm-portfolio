﻿using System;

namespace ATM_porfolio
{
    internal class Operation
    {
        // Withdraw function and to check if you can withdraw.
        public static Boolean withdraw(int _amount, Card _card)
        {
            if (validation.canWithdraw(_amount, _card) == true)
            {
                _card.removeBalance(_amount);
                return true;
            }
            return false;
        }

        // Check balance function return blance check
        public static string Balance(Card _card)
        {
            string _cardAmount = _card.GetBalanceFormated();
            return _cardAmount;
        }

        //Add deposit function
        public static void deposit(int _depositAmount, Card _card)
        {
            _card.addBalance(_depositAmount);
        }
    }
}