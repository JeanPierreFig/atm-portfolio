﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_porfolio
{
    internal class Card
    {
        //Jean Pierre Figaredo
        //5/12/17

        private decimal mBalance;
        public string mFirstName;
        public string mLastName;
        public int mId;

        public Card(int _id, string _firstName, string _lastName, decimal _balance)
        {
            //Create the card object.
            mBalance = _balance;
            mFirstName = _firstName;
            mLastName = _lastName;
            mId = _id;
        }

        public decimal GetBalance()
        {
            //return valid or invalid
            return mBalance;
        }

        public string GetBalanceFormated()
        {
            //return balance
            return mBalance.ToString("C");
        }

        public void addBalance(decimal _amount)
        {
            //Update the balance of the card.
            SetBalance(GetBalance() + _amount);
        }

        public void removeBalance(decimal _amount)
        {
            //set the balance plus the amount added.
            SetBalance(GetBalance() - _amount);
        }

        private void SetBalance(decimal _amount)
        {
            //set the balance to the class object.
            this.mBalance = _amount;
        }
    }
}