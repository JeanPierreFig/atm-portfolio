﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ATM_porfolio
{
    class Mysql
    {

        public static Card getCard(int _cardnumber, int _pin)
        {

            string cs = @"server=192.168.1.71;userid=SunBankAdmin ;password=password;database=SunBank; port=8889";

            MySqlConnection conn = null;

            try
            {
                conn = new MySqlConnection(cs);
                MySqlDataReader rdr = null;
                conn.Open();

                string stm = String.Format("select * from cards where cardnumber = {0} and pin = {1}",_cardnumber,_pin);
                MySqlCommand cmd = new MySqlCommand(stm, conn);

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        Console.WriteLine(rdr.GetInt32(0));
                        return new Card(rdr.GetInt32(0),rdr.GetString(3),rdr.GetString(4),rdr.GetInt32(5));
                    }
                }
            
               

            }
            catch (MySqlException ex)
            {

                Console.WriteLine("Error: {0}", ex.ToString());
            }

            return null;

        }

        public static bool SaveCard(Card _card)
        {

            string cs = @"server=192.168.1.71;userid=SunBankAdmin; password=password;database=SunBank; port=8889";


            try
            {

                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();
                MySqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update cards SET  balance = @newBalance where id = @id";
                comm.Parameters.AddWithValue("@id", _card.mId);
                comm.Parameters.AddWithValue("@newBalance", _card.GetBalance());
                comm.ExecuteNonQuery();
                conn.Close();

               
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

                return false;

            }


            return true;

        }




    }
}
