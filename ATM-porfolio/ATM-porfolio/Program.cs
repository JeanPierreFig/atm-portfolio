﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_porfolio
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Clear();
            Console.SetWindowSize(Math.Min(120, Console.LargestWindowWidth), Math.Min(40, Console.LargestWindowHeight));
            Console.BackgroundColor = ConsoleColor.Blue;
            WaitingScreen();
        }

        public static void WaitingScreen()
        {
            Interface.DrawLogo();
            EnterATM();
        }

        public static void EnterATM()
        {
            Card authenticatedCard = Interface.CardAndPinPrompt();

            while (authenticatedCard == null)
            {
                Interface.ErrorScreen("your card number or \r\n          pin was incorect.");
                authenticatedCard = Interface.CardAndPinPrompt();
            }

            loop(authenticatedCard);
        }

        public static void loop(Card _card)
        {
            switch (Interface.OperationPrompt())
            {
                case 1:
                    Console.Beep();
                    Interface.WithdrawMoney(_card);
                    break;

                case 2:
                    Console.Beep();
                    Interface.DepositMoney(_card);
                    break;

                case 3:
                    Console.Beep();
                    Interface.CheckBalance(_card);
                    break;

                case 4:
                    Console.Beep();
                    Console.Beep();
                    Interface.ExitATM(_card);

                    break;
            }
        }
    }
}