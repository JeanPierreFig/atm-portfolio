﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_porfolio
{
    internal class validation
    {
        /*Cale Unger
         * Validation Class
         * 5/11/17
         */

        public static bool ValidateCard(int _cardNumber, int _pin)
        {
            if (Mysql.getCard(_cardNumber, _pin) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateNumber(string _num)
        {
            int numCheck;

            while (!(int.TryParse(_num, out numCheck)))
            {
                return false;
            }
            return true;
        }

        public static int stringToNumber(string _num)
        {
            int numCheck;

            int.TryParse(_num, out numCheck);

            return numCheck;
        }

        public static bool canWithdraw(int _amount, Card _card)
        {
            if (_card.GetBalance() >= _amount)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}